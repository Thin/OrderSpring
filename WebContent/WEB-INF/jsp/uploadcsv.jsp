<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>CSV Importing..</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

<spring:url value="resources/css/csvupload.css" var="tableCSS" />
<link href="${tableCSS }" rel="stylesheet" />

<spring:url value="resources/images/" var="images" />

</head>
<body>

	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand"
				href="http://localhost:8080/OrderSpring/uploadCSV">Welcome @
				Product Information </a>
		</div>
	</div>
	</nav>

	<form id="multipartResolver" action="uploading" method="POST"
		enctype="multipart/form-data" class="location">
		<label for="file" class="custom-file-upload"><img
			src="${ images}/icon-csv.png" class="img">Upload CSV file
			Here..</label> <input id="file" type="file" name="file" class="submit" /><br>
		<p>
			<button type="submit" class="btn btn-info">Upload</button>
		</p>
	</form>

</body>
</html>