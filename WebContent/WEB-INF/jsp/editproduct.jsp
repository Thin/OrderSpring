<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<spring:url value="resources/css/editproduct.css" var="tableCSS" />
<link href="${tableCSS }" rel="stylesheet" />
<title>Insert title here</title>
<style>
.error {
	color: red;
	font-weight: bold;
}
</style>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

</head>
<body class="bg-22">

	<h2 class="h2">Edit Product</h2>

	<form:form method="POST" action="/OrderSpring/product2"
		commandName="product">
		<form:hidden path="id" />
		<table class="table">
			<tr>
				<td class="form"><form:label path="name"> Name</form:label></td>
				<td class="form"><form:input path="name" /></td>
				<td><form:errors path="name" cssClass="error" /></td>
			</tr>
			<tr>
				<td class="form"><form:label path="price">Price</form:label></td>
				<td class="form"><form:input path="price" /></td>
				<td><form:errors path="price" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="2" class="form"><input type="submit" value="Edit"
					class="btn btn-info" /></td>
			</tr>

		</table>
	</form:form>

</body>
</html>