<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Customers</title>
</head>
<body>
<table>
	<tr>
		<td align="center"><a href="http://localhost:8080/OrderSpring/addcustomer">Add Customer</a></td>
		</tr>
</table>
<table border="1" width="500" cellspacing="0">
	<tr align="center"><td>ID</td>
	<td>Name</td>
	<td>Address</td>
	<td colspan=2>Actions</td>
	</tr>
	
	<c:forEach var="customer" items="${customerList}">
	<tr align="center">
	<td>${customer.id }</td>
	<td>${customer.name }</td>
	<td>${customer.address }</td>
	<td><a href="http://localhost:8080/OrderSpring/editcustomer?id=${customer.id }">Edit</a></td>
	<td><a href="http://localhost:8080/OrderSpring/deletecustomer?id=${customer.id }" onclick="return confirm('r u sure to delete?')">Delete</a></td>
	</tr>
	</c:forEach>
</table>

</body>
</html>