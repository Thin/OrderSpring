<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h2>Edit Customer</h2>
<form:form method="POST" action="/OrderSpring/editingcustomer">
<form:hidden path="id"/>
<table>
	<tr>
		<td><form:label path="name">Name</form:label></td>
		<td><form:input path="name"/></td>
	</tr>
	<tr>
		<td><form:label path="address">Address</form:label></td>
		<td><form:input path="address"/></td>
	</tr>
	<tr>
		<td colspan=2><input type="submit" value="Edit"/></td>
	</tr>
</table>
</form:form>

</body>
</html>