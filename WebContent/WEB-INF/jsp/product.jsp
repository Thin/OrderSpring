<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>All Products</title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<spring:url value="resources/css/style.css" var="tableCSS" />
<link href="${tableCSS }" rel="stylesheet" />
<spring:url value="resources/images/" var="images" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>angular.module('myApp', []).controller('namesCtrl', function($scope) {});
</script>
</head>
<style>
input[type=text] {
    width: 50%;
    box-sizing: border-box;
    border: 2px solid #0000ff;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('searchicon.png');
    background-position: 10px 10px;
    background-repeat: no-repeat;
    padding: 10px 15px 10px 40px;
    margin-top:15px;
    margin-bottom: 15px;
    width: 150px;
     position: relative;
}
</style>
<body>

	<div class="bg-1">
		<div class="container text-center">

	<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="http://localhost:8080/OrderSpring/product">Welcome @ Product Information </a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="http://localhost:8080/OrderSpring/product">Home</a></li>
				</ul>
			</div>
	</nav>




<c:url value="/j_spring_security_logout" var="logoutUrl" />

	<!-- csrt for log out-->
	<form action="${logoutUrl}" method="post" id="logoutForm">
	  <input type="hidden" 
		name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	</form>
	
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h2>
			Welcome : ${pageContext.request.userPrincipal.name} | <a
				href="javascript:formSubmit()"> Logout</a>
		</h2>
	</c:if>









	<a href="http://localhost:8080/OrderSpring/addproduct" class="btn btn-info">Add New Product</a>
	<a href="http://localhost:8080/OrderSpring/downloadCSV" class="btn btn-info">Download CSV</a>
	<a href="http://localhost:8080/OrderSpring/uploadCSV" class="btn btn-info">Upload CSV</a>

	<div ng-app="myApp" ng-controller="namesCtrl" ng-init='names=${productList }'>

		<form><input type="text" ng-model="test" placeholder="Search.."></form>

		<table border="1" width="200" cellspacing="0" class="table table-striped">
			<tr align="center">
				<td class="tdcolor">ID</td>
			    <td class="tdcolor">Name</td>
				<td class="tdcolor">Price</td>
				<td colspan=2 class="tdcolor">Actions</td>
			</tr>

			<tr ng-repeat="x in names | filter:test">
				<td class="tdcolor">{{ x.id }}</td>
				<td class="tdcolor">{{ x.name }}</td>
				<td class="tdcolor">{{ x.price }}</td>
				<td><a href="http://localhost:8080/OrderSpring/editproduct?id={{ x.id }}"><img src="${ images}/edit_notes_delete.png" class="img">Edit</a></td>
				<td><a href="http://localhost:8080/OrderSpring/deleteproduct?id={{ x.id }}" onclick="return confirm('r u sure to delete?')"><img src="${images}/Editing-Delete-icon1.png" class="img">Delete</a></td>
			</tr>
		</table>  
	</div>

	<ul class="pagination">
		<c:if test="${currentPage != 1}">
			<li><a href="http://localhost:8080/OrderSpring/product?page=${currentPage - 1}">Previous</a></li>
		</c:if>
		
		<c:forEach begin="1" end="${noOfPages}" var="i">
			<c:choose>
				<c:when test="${currentPage eq i}">
					<li class="active"><a href="">${i}</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="http://localhost:8080/OrderSpring/product?page=${i}">${i}</a></li>
				</c:otherwise>
			</c:choose>
		</c:forEach>
			
		<c:if test="${currentPage lt noOfPages}">
			<li><a href="http://localhost:8080/OrderSpring/product?page=${currentPage + 1}">Next</a></li>
		</c:if>
	</ul> <br>
				
		</div>
	</div>
		
</body>
</html>