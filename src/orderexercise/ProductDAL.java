/**
 * @author myatmyolwin
 */

package orderexercise;


import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;


public class ProductDAL {
	private int noOfRecords;
	
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	public List<Product> getAll() throws SQLException, ClassNotFoundException {
		
		org.hibernate.Session session = sessionFactory.openSession();
		Query query = session.createQuery("from Product");
		List<Product> products = query.list();
		return products;

	}
//	
//	public List<Product> viewAllEmployees(int offset,int noOfRecords){
//		org.hibernate.Session session = sessionFactory.openSession();
//		Query query = session.createQuery("select SQL_CALC_FOUND_ROWS * from Product limit"+offset+","+noOfRecords);
//		List<Product> products=query.list();
//		return products;
//	}
//	
	public int getNoOfRecords(){
		return noOfRecords;
	}

	public void add(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(product);
		session.getTransaction().commit();
		session.close();
		
	}

	public void update(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(product);
		session.getTransaction().commit();
		session.close();

	}

	public void delete(Product product) throws SQLException, ClassNotFoundException {

		org.hibernate.Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(product);
		session.getTransaction().commit();
		session.close();

	}

}
