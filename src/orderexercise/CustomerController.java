package orderexercise;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

public class CustomerController {
	List<Customer> customerList;
	
	@Autowired
	private CustomerDAL customerDao;
	
	@RequestMapping(value="/customer",method=RequestMethod.GET)
	public ModelAndView customer(){
		customerList=new ArrayList<>();
		try {
			customerList=customerDao.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ModelAndView modelView=new ModelAndView("customer");
		modelView.addObject("customerList",customerList);
		return modelView;
	}
	
	@RequestMapping(value="/addcustomer",method=RequestMethod.GET)
	public ModelAndView addCustomer(){
		ModelAndView modelView=new ModelAndView("addcutomer","command",new Customer());
		return modelView;
	}
	@RequestMapping(value="/addingcustomer",method=RequestMethod.POST)
	public ModelAndView addingcustomer(@ModelAttribute("SpringWeb")Customer customer,ModelMap model){
		try {
			customerDao.add(customer);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customer();
	}
	
	@RequestMapping(value="/deletecustomer",method=RequestMethod.GET)
	public ModelAndView deleteCustomer(HttpServletRequest request, HttpServletResponse response)throws Exception{
		int id=Integer.parseInt(request.getParameter("id"));
		Customer customer=findCustomerById(id);
		customerDao.delete(customer);
		return customer();
	}
	
	@RequestMapping(value="/editcustomer",method=RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request,HttpServletResponse response)throws Exception{
		int id=Integer.parseInt(request.getParameter("id"));
		Customer customer=findCustomerById(id);
		ModelAndView modelView=new ModelAndView("editcustomer","command",customer);
		return modelView;
	}
	@RequestMapping(value="/editingcustomer",method=RequestMethod.POST)
	public ModelAndView editingcustomer(@ModelAttribute("SpringWeb")Customer customer,ModelMap model){
		try {
			customerDao.update(customer);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customer();
	}

	private Customer findCustomerById(int id) {
		// TODO Auto-generated method stub
		Customer customer=new Customer();
		
		for(Customer c:customerList){
			if(c.getId()==id){
				customer.setId(c.getId());
				customer.setName(c.getName());
				customer.setAddress(c.getAddress());
				
			}
		}
		return customer;
	}

}
