package orderexercise;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

@Controller
public class ProductController {

	List<Product> productList;

	@Autowired
	private ProductDAL productDao;
	
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String welcomePage() {

		
		return "login";

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView product(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		productList = new ArrayList<>();
		int page = 1;
		int recordsPerPage = 5;
		if (request.getParameter("page") != null)
			page = Integer.parseInt(request.getParameter("page"));

		try {
			productList = productDao.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int noOfRecords = productList.size();
		int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

		int startIndex = (page - 1) * recordsPerPage;
		int endIndex = startIndex + recordsPerPage;

		if (endIndex > productList.size()) {
			endIndex = productList.size();
		}
		JSONArray jarray=new JSONArray();
		List<Product> toSend=productList.subList(startIndex, endIndex);
		for(Product product:toSend){
			JSONObject json=new JSONObject();
			try {
				json.put("id", product.getId());
				json.put("name", product.getName());
				json.put("price",product.getPrice());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jarray.put(json);
		}
		
		
		

		ModelAndView modelView = new ModelAndView("product");
		modelView.addObject("title", "Spring Security Custom Login Form");
		modelView.addObject("message", "This is protected page!");
		modelView.setViewName("product");
		modelView.addObject("noOfPages", noOfPages);
		modelView.addObject("currentPage", page);
		modelView.addObject("productList", jarray.toString());
		return modelView;
		
	}

	@RequestMapping(value = "/addproduct", method = RequestMethod.GET)
	public ModelAndView addProduct() {

		ModelAndView modelView = new ModelAndView("addproduct", "command", new Product());
		return modelView;
	}

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public String product(@Validated Product product, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "addproduct";
		} else {
			try {
				productDao.add(product);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			return "redirect:product";
		}
	}

	@RequestMapping(value = "/editproduct", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {

		int id = Integer.parseInt(request.getParameter("id"));

		Product product = findProductById(id);

		ModelAndView modelView = new ModelAndView("editproduct", "product", product);
		return modelView;
	}

	@RequestMapping(value = "/product2", method = RequestMethod.POST)
	public String product2(@Validated Product product, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "editproduct";
		} else {
			try {
				productDao.update(product);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			return "redirect:product";
		}
	}

	@RequestMapping(value = "/deleteproduct", method = RequestMethod.GET)
	public String deleteProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {

		int id = Integer.parseInt(request.getParameter("id"));

		Product product = findProductById(id);
		try {
			productDao.delete(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		return "redirect:product";

	}

	@RequestMapping(value = "/uploadCSV", method = RequestMethod.GET)
	public ModelAndView uploadCSV(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ModelAndView modelView = new ModelAndView("uploadcsv");
		return modelView;
	}

	@RequestMapping(value = "/uploading", method = RequestMethod.POST)
	public String uploading(@RequestParam MultipartFile file, HttpServletRequest request, ModelMap model)
			throws IOException {
		String rootPath = request.getSession().getServletContext().getRealPath("/");
		File dir = new File(rootPath + File.separator + "file");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		File serverfile = new File(dir.getAbsoluteFile() + File.separator + file.getOriginalFilename());
		try {
			try (InputStream is = file.getInputStream();
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverfile))) {
				int i;
				// write file to server
				while ((i = is.read()) != -1) {
					stream.write(i);
				}
				stream.flush();
			}

		} catch (IOException e) {
			model.put("msg", "failed to process file because : " + e.getMessage());
			return "redirect:product";
		}

		BufferedReader br = null;
		String line = "";
		String splitBy = ",";
		ArrayList<Product> productList = new ArrayList<Product>();

		try {
			// String filename = file.getOriginalFilename();
			br = new BufferedReader(new FileReader(serverfile));
			while ((line = br.readLine()) != null) {
				String[] products = line.split(splitBy);
				Product product = new Product();

			//	String idString = products[0];
				String nameString = products[1];
				String priceString = products[2];
				try {
					// product.setId(Integer.parseInt(idString));
					product.setName(nameString);
					product.setPrice(Integer.parseInt(priceString));
				} catch (NumberFormatException ex) {

				}

				productList.add(product);

				try {
					productDao.add(product);
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e2) {
			e2.printStackTrace();
			return "redirect:uploadCSV";

		} // finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		// }
		return "redirect:product";
	}

	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletResponse response) throws IOException {
		String csvFileName = "products.csv";
		response.setContentType("text/csv");

		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment;filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);

		productList = new ArrayList<>();
		try {
			productList = productDao.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
		String[] header = { "id", "name", "price" };
		csvWriter.writeHeader(header);

		for (Product pro : productList) {
			csvWriter.write(pro, header);
		}
		csvWriter.close();
	}

	@Autowired
	@Qualifier("userValidator")
	private Validator validator;

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@ModelAttribute("product")
	public Product createProduct() {
		return new Product();
	}

	private Product findProductById(int id) {

		Product product = new Product();

		for (Product p : productList) {
			if (p.getId() == id) {
				product.setId(p.getId());
				product.setName(p.getName());
				product.setPrice(p.getPrice());
			}
		}

		return product;
	}

}
