package orderexercise;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import orderexercise.Product;
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		
		return Product.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "price.required");
		
		Product product=(Product)target;
		if(product.getPrice()==0){
			errors.rejectValue("price", "price.required");
		}
		
		if(product.getName().length()<=7){
			errors.rejectValue("name", "name.short");
		}
		
	}

}
